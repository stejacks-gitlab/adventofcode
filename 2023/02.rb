# frozen_string_literal: true

module Day02
  class << self
    def dice_count_game(game)
      counts = { 'red' => 0, 'green' => 0, 'blue' => 0 }
      id = game.split(':')[0].split[1]
      game.split(':')[1].split(';').each do |round|
        round.split(',').each do |dice|
          count, colour = dice.split(' ')
          counts[colour] = count.to_i if count.to_i > counts[colour]
        end
      end
      [id, counts]
    end

    def part_one(input)
      maxcounts = { 'red' => 12, 'green' => 13, 'blue' => 14 }
      sum = 0
      possiblegame = false
      input.each do |line|
        id, counts = dice_count_game(line)
        if counts['red'] <= maxcounts['red'] && counts['green'] <= maxcounts['green'] && counts['blue'] <= maxcounts['blue']
          possiblegame = true
        else
          possiblegame = false
        end
        sum += id.to_i if possiblegame
      end
      sum
    end

    def part_two(input)
      sum = 0
      power = 0
      input.each do |line|
        _, counts = dice_count_game(line)
        power = counts['red'] * counts['green'] * counts['blue']
        sum += power
      end
      sum
    end
  end
end

module Day04
  class << self
    def part_one(input)
      results = []
      sum = 0
      input.each do | card |
      numbersmatch = 0
      points = 0
      id = card.split(':')[0].split[1]
      winning, mynumbers = card.split(':')[1].split('|')
      winning.split(" ").each do | number |
        if mynumbers.split(" ").include?(number)
          numbersmatch += 1
        end
      end
      if numbersmatch < 2
        points = numbersmatch
      else
        points = 2 ** (numbersmatch - 1)
      end
      results.push({"id" => id, "matched" => numbersmatch, "points" => points})
    end
    results.each do | card |
      sum += card["points"]
    end
    sum
    end

    def part_two(input)
      results = []
      sum = 0
      input.each do | card |
      numbersmatch = 0
      id = card.split(':')[0].split[1]
      winning, mynumbers = card.split(':')[1].split('|')
      winning.split(" ").each do | number |
        if mynumbers.split(" ").include?(number)
          numbersmatch += 1
        end
      end
      results.push({"matched" => numbersmatch, "copies" => 1})
      end
    results.each_with_index do | card, index |
      if card["matched"] > 0
        (1..card["copies"]).each do |j|
        (1..card["matched"]).each do |i|
          results[index + i]["copies"] += 1
        end
      end
      end
    end 
    results.each do |card|
       sum += card["copies"]
    end
    sum
    end
  end
end

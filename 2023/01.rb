# frozen_string_literal: true

module Day01
  class << self
    def replace_numbers(line)
      %w[one two three four five six seven eight nine].each_with_index do |word, index|
        line = line.gsub(word, word[0] + (index + 1).to_s + word[1..])
      end
      line
    end

    def return_first_and_last(line)
      numbers = line.scan(/\d/)
      "#{numbers.first}#{numbers.last}".to_i
    end

    def part_one(input)
      sum = 0
      input.each do |line|
        sum += return_first_and_last(line)
      end
      sum
    end

    def part_two(input)
      sum = 0
      input.each do |line|
        newline = replace_numbers(line)
        sum += return_first_and_last(newline)
      end
      sum
    end
  end
end

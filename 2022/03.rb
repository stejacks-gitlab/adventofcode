# frozen_string_literal: true

module Day03
  class Rucksack
    attr_reader :item

    def split(contents)
      characters = contents.chars
      half = characters.size / 2
      compartment1 = characters.slice(0, half)
      compartment2 = characters.slice(half, characters.size)
      [compartment1, compartment2]
    end

    def duplicate?(compartments)
      compartment1 = compartments[0]
      compartment2 = compartments[1]
      compartment1 & compartment2
    end

    def duplicate_elves?(elf1, elf2, elf3)
      elf1.chars & elf2.chars & elf3.chars
    end

    def priority(duplicate_item)
      priorities = (('a'..'z').zip((1..26)) + ('A'..'Z').zip((27..52))).to_h
      priorities[duplicate_item]
    end
  end
  class << self
    def part_one(input)
      rucksack = Rucksack.new
      total_priority = 0
      input.each do |line|
        compartments = rucksack.split(line)
        duplicate_item = rucksack.duplicate?(compartments)
        priority = rucksack.priority(duplicate_item[0])
        total_priority += priority
      end
      puts total_priority
    end

    def part_two(input)
      rucksack = Rucksack.new
      total_priority = 0
      input.each_slice(3) do |elf1, elf2, elf3|
        badge = rucksack.duplicate_elves?(elf1, elf2, elf3)
        priority = rucksack.priority(badge[0])
        total_priority += priority
      end
      puts total_priority
    end
  end
end

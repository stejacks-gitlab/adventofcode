# frozen_string_literal: true

module Day05
  class Cargo
    attr_accessor :stacks

    def initialize
      @stacks = { 1 => [], 2 => [], 3 => [], 4 => [], 5 => [], 6 => [], 7 => [], 8 => [], 9 => [] }
    end

    def move(number, stack_start, stack_end)
      move = 1
      while move <= number
        stacks[stack_start].push(stacks[stack_end].pop)
        move += 1
      end
    end

    def move_9001(number, stack_start, stack_end)
      stacks[stack_end].pop(number).each do |container|
        stacks[stack_start].push(container)
      end
    end
  end

  class << self
    def part_one(input)
      cargo = Cargo.new
      input.each do |line|
        case line
        when /\[/
          chars = line.chars
          stack = 1
          index = 1
          while index <= chars.length
            cargo.stacks[stack].unshift(chars[index]) unless chars[index] == ' '
            stack += 1
            index += 4
          end
        when 'move'
          words = line.split(/\W+/)
          number = words[1].to_i
          stack_end = words[3].to_i
          stack_start = words[5].to_i
          cargo.move(number, stack_start, stack_end)
        end
      end
      answer = ''
      cargo.stacks.each do |stack|
        answer += stack[1].last
      end
      puts answer
    end

    def part_two(input)
      cargo = Cargo.new
      input.each do |line|
        case line
        when /\[/
          chars = line.chars
          stack = 1
          index = 1
          while index <= chars.length
            cargo.stacks[stack].unshift(chars[index]) unless chars[index] == ' '
            stack += 1
            index += 4
          end
        when 'move'
          words = line.split(/\W+/)
          number = words[1].to_i
          stack_end = words[3].to_i
          stack_start = words[5].to_i
          cargo.move_9001(number, stack_start, stack_end)
        end
      end
      answer = ''
      cargo.stacks.each do |stack|
        answer += stack[1].last
      end
      puts answer
    end
  end
end

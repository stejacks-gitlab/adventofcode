class Elf
  @@number_of_elfs = 0
  attr_reader :name, :calories

  def initialize
    @calories = 0
    @@number_of_elfs += 1
    @name = @@number_of_elfs
  end

  def add_calories(more_calories)
    @calories += more_calories
  end
end

# frozen_string_literal: true

require_relative 'elf'

module Day01
  class << self
    def part_one(input)
      @elves = []
      current_elf = Elf.new
      @elves << current_elf
      input.each do |line|
        if line == ''
          current_elf = Elf.new
          @elves << current_elf
        else
          current_elf.add_calories(line.to_i)
        end
      end
      max_elf = @elves.max_by(&:calories)
      puts max_elf.calories
    end

    def part_two(_input)
      max_elf = @elves.max_by(3, &:calories)
      total_calories = 0
      max_elf.each do |elf|
        total_calories += elf.calories
      end
      puts total_calories
    end
  end
end

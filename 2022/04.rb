# frozen_string_literal: true

module Day04
  class Sections
    def convert_sections(elf)
      sections = elf.split('-')
      section_start = sections[0].to_i
      section_end = sections[1].to_i
      [*section_start..section_end].to_a
    end

    def overlap_completely?(elf1, elf2)
      return true if elf1 - elf2 == [] || elf2 - elf1 == []

      false
    end

    def overlap_at_all?(elf1, elf2)
      return true if elf1 & elf2 != []

      false
    end
  end

  class << self
    def part_one(input)
      section = Sections.new
      pairs = 0
      input.each do |line|
        sections = line.split(',')
        elf1 = sections[0]
        elf2 = sections[1]
        elf1_section = section.convert_sections(elf1)
        elf2_section = section.convert_sections(elf2)
        pairs += 1 if section.overlap_completely?(elf1_section, elf2_section)
      end
      puts pairs
    end

    def part_two(input)
      section = Sections.new
      pairs = 0
      input.each do |line|
        sections = line.split(',')
        elf1 = sections[0]
        elf2 = sections[1]
        elf1_section = section.convert_sections(elf1)
        elf2_section = section.convert_sections(elf2)
        pairs += 1 if section.overlap_at_all?(elf1_section, elf2_section)
      end
      puts pairs
    end
end
end

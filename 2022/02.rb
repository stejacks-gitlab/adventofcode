# frozen_string_literal: true

class RockPaperScissors
  attr_reader :points

  def initialize
    @points = 0
  end

  def play(opponent, you)
    you_name = name(you)
    opponent_name = opponent_name(opponent)
    result = win?(opponent_name, you_name)
    score(you_name, result)
  end

  def play2(opponent, result)
    result_name = name2(result)
    opponent_name = opponent_name(opponent)
    you_name = result?(opponent_name, result_name)
    score(you_name, result_name)
  end

  def opponent_name(opponent)
    case opponent
    when 'A'
      'rock'
    when 'B'
      'paper'
    else
      'scissors'
    end
  end

  def name2(result)
    case result
    when 'X'
      'loss'
    when 'Y'
      'draw'
    else
      'win'
    end
  end

  def name(you)
    case you
    when 'X'
      'rock'
    when 'Y'
      'paper'
    else
      'scissors'
    end
  end

  def win?(opponent, you)
    result = ''
    result = 'draw' if opponent == you
    case opponent
    when 'rock'
      case you
      when 'scissors'
        result = 'loss'
      when 'paper'
        result = 'win'
      end
    when 'scissors'
      case you
      when 'paper'
        result = 'loss'
      when 'rock'
        result = 'win'
      end
    when 'paper'
      case you
      when 'rock'
        result = 'loss'
      when 'scissors'
        result = 'win'
      end
    end
    result
  end

  def score(you, result)
    case result
    when 'win'
      @points += 6
    when 'draw'
      @points += 3
    end

    @points += case you
               when 'rock'
                 1
               when 'paper'
                 2
               else
                 3
               end
  end

  def result?(opponent, result)
    you = opponent if result == 'draw'
    case opponent
    when 'rock'
      case result
      when 'loss'
        you = 'scissors'
      when 'win'
        you = 'paper'
      end
    when 'scissors'
      case result
      when 'loss'
        you = 'paper'
      when 'win'
        you = 'rock'
      end
    when 'paper'
      case result
      when 'loss'
        you = 'rock'
      when 'win'
        you = 'scissors'
      end
    end
    you
  end
end

module Day02
  class << self
    def part_one(input)
      game = RockPaperScissors.new
      input.each do |line|
        answer = line.split(/\W+/)
        opponent = answer[0]
        you = answer[1]
        game.play(opponent, you)
      end
      puts game.points
    end

    def part_two(input)
      game = RockPaperScissors.new
      input.each do |line|
        answer = line.split(/\W+/)
        opponent = answer[0]
        result = answer[1]
        game.play2(opponent, result)
      end
      puts game.points
    end
  end
end

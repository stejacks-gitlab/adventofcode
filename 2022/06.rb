# frozen_string_literal: true

module Day06
  class << self
    def signal(input, message_length)
      signal = input[0].chars
      position = 0
      while position < signal.length
        signal_slice = signal.slice(position, message_length)
        break if signal_slice == signal_slice.uniq

        position += 1
      end
      position + message_length
    end

    def part_one(input)
      message_length = 4
      puts signal(input, message_length)
    end

    def part_two(input)
      message_length = 14
      puts signal(input, message_length)
    end
  end
end
